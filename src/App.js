import React, { useEffect, useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import ProductList from "./pages/products-list/ProductsList";
import CollectionsList from "./pages/collections-list/CollectionsList";
import CartPage from "./pages/cart-page/CartPage";
import NavBar from "./components/nav/NavBar";
import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  allCollectionsQuery,
  createCheckout,
  checkoutLineItemsAdd,
  checkoutLineItemsRemove,
  checkoutLineItemsUpdate,
  fetchCheckout,
} from "./queries";

function App() {
  const { loading: shopLoading, error: shopError, data: shopData } = useQuery(
    allCollectionsQuery
  );

  const [checkout, setCheckout] = useState({ lineItems: { edges: [] } });
  const [checkoutId, setCheckoutId] = useState(
    localStorage.getItem("shopify_checkout_id")
  );
  const {
    loading: localCheckoutLoading,
    error: localCheckoutError,
    data: localCheckoutData,
  } = useQuery(fetchCheckout, { variables: { id: checkoutId } });

  const [
    createCheckoutMutation,
    {
      data: createCheckoutData,
      loading: createCheckoutLoading,
      error: createCheckoutError,
    },
  ] = useMutation(createCheckout);

  const [
    lineItemAddMutation,
    {
      data: lineItemAddData,
      loading: lineItemAddLoading,
      error: lineItemAddError,
    },
  ] = useMutation(checkoutLineItemsAdd);

  const [
    lineItemRemoveMutation,
    {
      data: lineItemRemoveData,
      loading: lineItemRemoveLoading,
      error: lineItemRemoveError,
    },
  ] = useMutation(checkoutLineItemsRemove);

  const [
    lineItemUpdateMutation,
    {
      data: lineItemUpdateData,
      loading: lineItemUpdateLoading,
      error: lineItemUpdateError,
    },
  ] = useMutation(checkoutLineItemsUpdate);

  const createNewCheckout = () => {
    const variables = { input: {} };
    createCheckoutMutation({ variables }).then(
      (res) => {
        console.log("created new checkout");
        setCheckoutInState(res.data.checkoutCreate.checkout);
      },
      (err) => {
        console.log("create checkout error", err);
      }
    );
  };

  const setCheckoutInState = (checkout) => {
    localStorage.setItem("shopify_checkout_id", checkout.id);
    setCheckoutId(checkout.id);
  };

  useEffect(() => {
    if (!checkoutId) {
      createNewCheckout();
    }
  }, []);

  useEffect(() => {
    if (localCheckoutData) setCheckout(localCheckoutData.node);
  }, [localCheckoutData]);

  const addVariantToCart = (variantId, quantity) => {
    const variables = {
      checkoutId: localCheckoutData.node.id,
      lineItems: [{ variantId, quantity: parseInt(quantity, 10) }],
    };
    console.log(variables, "variables");

    lineItemAddMutation({ variables })
      .then((res) => {
        console.log("added to cart", res);
        setCheckout(res.data.checkoutLineItemsAdd.checkout);
      })
      .catch((e) => console.log(e));
  };

  const removeLineItemInCart = (lineItemId) => {
    const variables = {
      checkoutId: localCheckoutData.node.id,
      lineItemIds: [lineItemId],
    };
    lineItemRemoveMutation({ variables })
      .then((res) => {
        console.log("removed from cart", res);
        setCheckout(res.data.checkoutLineItemsRemove.checkout);
      })
      .catch((e) => console.log(e));
  };

  const updateLineItemInCart = (lineItemId, quantity) => {
    const variables = {
      checkoutId: localCheckoutData.node.id,
      lineItems: [{ id: lineItemId, quantity: parseInt(quantity, 10) }],
    };
    lineItemUpdateMutation({ variables })
      .then((res) => {
        console.log("updated cart", res);
        setCheckout(res.data.checkoutLineItemsUpdate.checkout);
      })
      .catch((e) => console.log(e));
  };

  if (shopLoading | shopError | localCheckoutLoading) {
    return <h1>Loading</h1>;
  }

  return (
    <>
      <BrowserRouter>
        <NavBar />
        <Switch>
          <Route
            exact
            path="/"
            render={() => <CollectionsList data={shopData} />}
          />
          <Route
            exact
            path="/cart"
            render={() => (
              <CartPage
                checkout={checkout}
                removeFromCart={removeLineItemInCart}
                updateCart={updateLineItemInCart}
              />
            )}
          />
          <Route
            exact
            path="/:collection"
            render={(routeProps) => (
              <ProductList
                {...shopData}
                {...routeProps}
                addToCart={addVariantToCart}
                checkoutObj={checkout}
              />
            )}
          />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
