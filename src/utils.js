import { useEffect } from 'react';

export function useCheckoutEffect(data, key, setDataCallback) {
  useEffect(() => {
    if (data && data[key] && data[key].checkout) {
      setDataCallback(data[key].checkout);
    }
  }, [data]);
}