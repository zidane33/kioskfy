import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';

function LineItem(props){
    console.log(props)

  const decrementQuantity = (lineItemId) => {
    const updatedQuantity = props.line_item.quantity - 1
    props.updateLineItemInCart(lineItemId, updatedQuantity);
  }

  const incrementQuantity = (lineItemId) => {
    const updatedQuantity = props.line_item.quantity + 1
    props.updateLineItemInCart(lineItemId, updatedQuantity);
  }

  return (  
    <ListGroup.Item>
      <ListGroup horizontal>
      <ListGroup.Item>
        {props.line_item.variant.image ? <img className="image" src={props.line_item.variant.image.src} alt={`${props.line_item.title} product shot`}/> : null}
      </ListGroup.Item>
        <ListGroup.Item>
          <span>
            {props.line_item.title}
          </span>
        </ListGroup.Item>
        <ListGroup.Item>
          <Button variant="outline-primary" onClick={() => decrementQuantity(props.line_item.id)}>-</Button>
          <span>{props.line_item.quantity}</span>
          <Button variant="outline-primary" onClick={() => incrementQuantity(props.line_item.id)}>+</Button>
        </ListGroup.Item>
        <ListGroup.Item>
          <span >
            Price: ${ (props.line_item.quantity * props.line_item.variant.price).toFixed(2) }
          </span>
        </ListGroup.Item>
        <ListGroup.Item >
          <Button variant="outline-primary" onClick={()=> props.removeLineItemInCart(props.line_item.id)}>Remove From Cart</Button>
        </ListGroup.Item>
      </ListGroup>
    </ListGroup.Item>
  );
}

export default LineItem;