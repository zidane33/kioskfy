import React from "react";
import Nav from "react-bootstrap/Nav";
import { NavLink } from 'react-router-dom';
import './nav-bar.scss'

const NavBar = () => (
  <Nav
    activeKey="/home"
  >
      <Nav.Item className="nav-link">
          <NavLink to="/">
            Home
          </NavLink>
      </Nav.Item>
      <Nav.Item className="nav-link">
        <NavLink to="/cart">
          Cart
        </NavLink>
      </Nav.Item>
  </Nav>
);

export default NavBar;
