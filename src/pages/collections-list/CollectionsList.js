import React from "react";
import { Link } from 'react-router-dom';
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import "./collections-list.scss";

const CollecionsList = (props) => {
  const { edges } = props.data.collections;
  return (
    <div className="collections-list-container">
      <h1 className="title">Collections List</h1>
      <CardDeck>
        {edges.map((collection, idx) => (
          <Link key={idx} to={`/${collection.node.handle}`}>
            <Card>
              <Card.Img className="image" variant="top" src={collection.node.products.edges[0].node.variants.edges[0].node.image.originalSrc} />
              <Card.Body>
                <Card.Title>{collection.node.handle}</Card.Title>
                <Card.Text></Card.Text>
              </Card.Body>
            </Card>
          </Link>
        ))}
      </CardDeck>
    </div>
  );
};
export default CollecionsList;
