import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import LineItem from '../../components/line-item/LineItem';
import './cart-page.scss'

const CartPage = (props) => {
      let line_items = props.checkout.lineItems.edges.map((line_item) => {
        return (
          <LineItem
            removeLineItemInCart={props.removeFromCart}
            updateLineItemInCart={props.updateCart}
            key={line_item.node.id.toString()}
            line_item={line_item.node}
          />
        );
      });
    
      return (
        <div className="container">
          <header>
            <h2>Your cart</h2>
          </header>
          <ListGroup className="cart-wrapper">
            {line_items}
          </ListGroup>
          <ListGroup horizontal className="price-bar">
            <ListGroup.Item>
              <div>Subtotal</div>
              <div>
                <span>$ {props.checkout.subtotalPrice}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item >
              <div >Taxes</div>
              <div >
                <span >$ {props.checkout.totalTax}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div >Total</div>
              <div >
                <span>$ {props.checkout.totalPrice}</span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
                <a href={props.checkout.webUrl}>Checkout</a>
            </ListGroup.Item>
          </ListGroup>
        </div>
      )
    }

export default CartPage;