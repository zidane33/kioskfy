import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import Button from "react-bootstrap/Button";
import "./product-list.scss";

const ProductList = (props) => {
  const { edges: collections } = props.collections;
  const collectionTitle = props.match.params.collection;

  const collection = collections.find(
    (collectionName) => collectionName.node.handle === collectionTitle
  );
  const [filtersList, setFiltersList] = useState([]);

  if (!collection) return <h1>No Collection found</h1>;

  const products = collection.node.products.edges;
  const addToFilterList = (filter) => {
    if (filtersList.includes(filter)) return;
    setFiltersList([...filtersList, filter]);
  };

  const clearFilters = () => {
    setFiltersList([]);
  };

  return (
    <>
      <div className="filter-menu-wrapper">
        <h3>Filter menu</h3>
        <Button variant="outline-primary" value="pink" onClick={(e) => addToFilterList(e.target.value)}>
          pink
        </Button>
        <Button variant="outline-primary" value="black" onClick={(e) => addToFilterList(e.target.value)}>
          black
        </Button>
        <Button variant="outline-primary" onClick={() => clearFilters()}>Clear All</Button>
      </div>
      <div className="product-list-container">
        <h1>{collectionTitle}</h1>
        <CardDeck>
          {products
            .filter((product) =>
              filtersList.every((tag) => product.node.tags.includes(tag))
            )
            .map((product, idx) => (
              <Card key={idx}>
                <Card.Img
                  className="image"
                  variant="top"
                  src={product.node.variants.edges[0].node.image.originalSrc}
                />
                <Card.Body>
                  <Card.Title>{product.node.handle}</Card.Title>
                  <Card.Text as="div"></Card.Text>
                </Card.Body>
                <Card.Footer
                  onClick={() =>
                    props.addToCart(product.node.variants.edges[0].node.id, 1)
                  }
                >
                  Add to Cart
                </Card.Footer>
              </Card>
            ))}
        </CardDeck>
      </div>
    </>
  );
};
export default ProductList;
